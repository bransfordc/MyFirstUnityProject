﻿#pragma strict

var rb2D: Rigidbody2D; //Calls Rigid Body 2d

var target: Rigidbody2D; // The player's RB

var parallax = 1.00;

var xOffset = 0.00;
var yOffset = 0.00;

function Start () {

    rb2D = GetComponent.<Rigidbody2D>();

    target = GameObject.FindWithTag("Player").GetComponent.<Rigidbody2D>();

}

function Update () {

    rb2D.position.x = ( target.position.x * -parallax ) + xOffset;
    rb2D.position.y = ( target.position.y * -parallax ) + yOffset;
}
