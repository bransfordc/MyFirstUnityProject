﻿#pragma strict

//var xSpeed=0; // Speed of horizontal Movement
var thisLevel = 0; // Your Current Level
var timer=0; // Time component
var jump=8; // Jumping Variable
var onGround=2; // Allows you to jump
var spinSpeed=300; // How fast you spin
var spin = 0; // change in ds
var rb2D: Rigidbody2D; //Calls Rigid Body 2d
var dy=0;
var ds=0; // Rate that spinning changes
var shrinkTime = 0;
var stars=0; //How many stars you have collected
var movie: Animator; //
var clock=0;
var jumpTimer = 0; // Delays jumps
var alive = true;
var win = 0; //Trigger after you win a level
var eye: GameObject;
public var uiText: UI.Text;
public var uiText2: UI.Text;

// Win screen
public var winOrLose: UI.Text;
public var scores: UI.Text;
public var totalScore: UI.Text;
static var hsPosition = 5000;


static var state = ""; // What is your current condition



// Text Score Function
var textScore = function(){
    uiText.text=""+stars;
    uiText2.text=""+timer;
};

// Collision Enter Triggers
function OnTriggerEnter2D(other: Collider2D) {

    // Are you on the ground
    if (other.CompareTag ("ground")) {
        onGround=2;
    }    

    // Collects Stars
    if (other.CompareTag ("star")) {
        Destroy (other.gameObject);
        stars+=20;
    }

    // Kills Player
    if (other.CompareTag ("killer")) {
        //Destroy (gameObject);
        transform.localScale.x = 1;
        transform.localScale.y = 1;
        Destroy (eye);
        shrinkTime = 0;
        state = "lose";
        if(rb2D.gravityScale < 0){
            rb2D.MoveRotation(180);
            rb2D.gravityScale= -0.01;
        } else {
            rb2D.MoveRotation(0);
            rb2D.gravityScale= 0.01;
        }
        rb2D.angularDrag=1000;
        rb2D.drag=1000;
        alive = false;
        movie.SetInteger("living", 10); //Activates fall Animation
        ds=0;
    }


    // Win Zone
    if (other.CompareTag ("finishLine")) {
        state = "win";
        shrinkTime = 0;
        transform.localScale.x = 1;
        transform.localScale.y = 1;
        if(rb2D.gravityScale < 0){
            rb2D.MoveRotation(180);
        } else {
            rb2D.MoveRotation(0);
        }
        rb2D.velocity.x = 0;
        rb2D.velocity.y = 0;
        rb2D.gravityScale=0;
        rb2D.angularDrag=1000;
        rb2D.drag=1000;
        rb2D.MoveRotation(0);
        alive = false;
        movie.SetInteger("winner", 10); //Activates WIN Animation
        win=10;
        ds=0;
        
    }

    // Anti - Gravity
    if (other.CompareTag ("antiG")) {
        rb2D.gravityScale= -1;
    }
    // Regular - Gravity
    if (other.CompareTag ("regG")) {
        rb2D.gravityScale= 1;
    }
}

// Collision Stay Triggers
function OnTriggerStay2D(other: Collider2D) {
    // Bounce Left
    if ( other.CompareTag ("bounceL") ) {
        if( rb2D.gravityScale > 0 ){
            rb2D.velocity.x = Mathf.Abs(rb2D.velocity.x)*-0.8 - 0.5;
            ds = Mathf.Abs(ds) * -0.9;
            rb2D.MoveRotation((Mathf.Abs(rb2D.rotation) * -1) - ds * Time.fixedDeltaTime);
        }
        if( rb2D.gravityScale < 0 ){
            rb2D.velocity.x = Mathf.Abs(rb2D.velocity.x)*-0.8 - 0.5;
            ds = Mathf.Abs(ds) * 0.9;
            rb2D.MoveRotation(Mathf.Abs(rb2D.rotation) - ds * Time.fixedDeltaTime);
        }
    }
    // Bounce Right
    if ( other.CompareTag ("bounceR") ) {
        if( rb2D.gravityScale > 0 ){
            rb2D.velocity.x = Mathf.Abs(rb2D.velocity.x)*0.8 + 0.5;
            ds = Mathf.Abs(ds) * 0.9;
            rb2D.MoveRotation(Mathf.Abs(rb2D.rotation) - ds * Time.fixedDeltaTime);
        }
        if( rb2D.gravityScale < 0 ){
            rb2D.velocity.x = Mathf.Abs(rb2D.velocity.x)*0.8 + 0.5;
            ds = Mathf.Abs(ds) * -0.9;
            rb2D.MoveRotation((Mathf.Abs(rb2D.rotation) * -1) - ds * Time.fixedDeltaTime);
        }
    }



    // Immediate Death
    if ( other.CompareTag ("death") ) {
        movie.SetInteger("destroyed", 50); //Go to Nothing Animation
        transform.localScale.x = 1;
        transform.localScale.y = 1;
        Destroy (eye);
        shrinkTime = 0;
        state = "lose";
        rb2D.MoveRotation(0);
        rb2D.gravityScale= 0.01;
        rb2D.angularDrag=1000;
        rb2D.drag=1000;
        alive = false;
        ds=0;
    }

    
}

function Start () {
    rb2D = GetComponent.<Rigidbody2D>();
    movie= GetComponent.<Animator>();
    stars=0;
    textScore();
    clock=0;
    alive = true;
    hsPosition = 5000;
    state = "";
}

function Update () {
    textScore();


    //Did you get a high Score?
    if( state === "win" && (stars + timer) > Save.highScores[thisLevel - 1] ){
        Save.highScores[thisLevel - 1] = (stars + timer);
        hsPosition = 0;
    }


    // How long are you small?
    if( shrinkTime <= 0 ){
        transform.localScale.x = 1;
        transform.localScale.y = 1;
    }else {
        shrinkTime --;
    }

    // Win and Lose Screen
    if( state === "win" ){
        winOrLose.text = "YOU WIN!";
        scores.text = " : " + stars + "\n\n: " + timer;
        totalScore.text = ": " + (stars + timer);
    } else {
        winOrLose.text = "You Lost";
        scores.text = " : " + stars + "\n\n: " + timer;
        totalScore.text = ": 0" ;
    }


    // Timer
    clock++;
    if(clock>30 && timer>0 && alive===true && win<1){
        timer-=1;
        clock=0;
    }

    // Jumping
    if (alive === true && onGround > 0) {
        if( Input.GetAxis("Vertical") > 0 && rb2D.gravityScale > 0 && jumpTimer <= 0 ){
            rb2D.velocity.y = jump;
            onGround--;
            jumpTimer = 20;
        }
        if( Input.GetAxis("Vertical") < 0 && rb2D.gravityScale < 0 && jumpTimer <= 0 ){
            rb2D.velocity.y = -jump;
            onGround--;
            jumpTimer = 20;
        }
    }


    //Move Left & Right
    //var dx = Input.GetAxis("Horizontal") * Time.deltaTime * xSpeed;
    //transform.position.x += dx;

    //Spinning
    if( alive === true ){
        //spin = Input.GetAxis("Horizontal") * Time.deltaTime * spinSpeed; // <-- for computer
        ds+=spin;
        rb2D.MoveRotation(rb2D.rotation -ds * Time.fixedDeltaTime); 
    }
    

    // Spinning Constraints
    var cap=600;
    if(ds>cap){
        ds=cap;
    }
    if(ds<-cap){
        ds=-cap;
    }
    
    if(ds<0){
        ds+=2;
    } else if(ds>0){
        ds-=2;
    }

    if(ds<2 && ds>-2){
        ds=0;
    }

    //Animation Var
    movie.SetInteger("RollSpeed", ds); //Makes RollSpeed equal ds



}



public function RBDown(){
    if( alive === true ){
        spin += Time.deltaTime * spinSpeed;
    }
}

public function RBUp(){
    spin = 0;
}

public function LBDown(){
    if( alive === true ){
        spin = Time.deltaTime * -spinSpeed;
    }
}

public function LBUp(){
        spin = 0;
}

public function UpButton(){
    if ( alive === true && onGround > 0 && rb2D.gravityScale > 0) {
        rb2D.velocity.y = jump;
        onGround--;
    }
    if ( state === "" && rb2D.gravityScale < 0) {
        transform.localScale.x = 0.4;
        transform.localScale.y = 0.4;
        shrinkTime = 200;
    }
}

public function DownButton(){
    if ( alive === true && onGround > 0 && rb2D.gravityScale < 0) {
        rb2D.velocity.y = -jump;
        onGround--;
    }
    if ( state === "" && rb2D.gravityScale > 0) {
        transform.localScale.x = 0.4;
        transform.localScale.y = 0.4;
        shrinkTime = 200;
    }
}