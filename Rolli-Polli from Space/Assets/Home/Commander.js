﻿#pragma strict

static var scrollY = 0.00;

var scrollSpeed = 0;

function Start () {
    Player.state = "";
}

function Update () {
    scrollY += scrollSpeed;

    if( scrollY < 0 ){
        scrollY = 0;
    }

}

// Scrolling functions
function scrollUp (){
    scrollSpeed = -10;
}
function scrollDown (){
    scrollSpeed = 10;
}
function scrollZero (){
    scrollSpeed = 0;
}


// Go to HomePage
function goHome (){
    Application.LoadLevel("0)Homepage");
}

// Go to Levels
function level1 (){
    Application.LoadLevel("1)Level1");
}
function level2 (){
    Application.LoadLevel("2)Level2");
}
function level3 (){
    Application.LoadLevel("3)Level3");
}
function level4 (){
    Application.LoadLevel("4)Level4");
}
function level5 (){
    Application.LoadLevel("5)Level5");
}

// Go to Albums
function album1 (){
    Application.LoadLevel("Album1");
}