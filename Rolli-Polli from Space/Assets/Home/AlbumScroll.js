﻿#pragma strict

var move = 0;
static var delta = 0; // how much everything's moved over
var pic = 0; // which picture you're on
var end = 6; // end of the album before you go home

function Start () {
    move = 0;
}

function Update () {

    if( move > 0 ){
        move --;
        //transform.localPosition.x -= 10;
        delta -= 20;
    }

    if( move < 0 ){
        move ++;
        //transform.localPosition.x += 10;
        delta += 20;
    }

    if( pic >= 6 ){
        Application.LoadLevel("0)Homepage");
        delta = 0;
    }

}

// Buttons
function goLeft(){
    if( move == 0 && pic > 0 ){
        move = -50;
        pic --;
    }
}
function goRight(){
    if( move == 0 ){
        move = 50;
        pic ++;
    }
}