﻿#pragma strict

// Level High Scores
static var highScores = [0, 0, 0, 0, 0];

// My functions

function Start () {
    highScores[0] = (PlayerPrefs.GetInt("LevelScore1"));
    highScores[1] = (PlayerPrefs.GetInt("LevelScore2"));
    highScores[2] = (PlayerPrefs.GetInt("LevelScore3"));
    highScores[3] = (PlayerPrefs.GetInt("LevelScore4"));
    highScores[4] = (PlayerPrefs.GetInt("LevelScore5"));
}

function Update () {
    if( Player.state === "win" ){
        PlayerPrefs.SetInt("LevelScore1", highScores[0]);
        PlayerPrefs.SetInt("LevelScore2", highScores[1]);
        PlayerPrefs.SetInt("LevelScore3", highScores[2]);
        PlayerPrefs.SetInt("LevelScore4", highScores[3]);
        PlayerPrefs.SetInt("LevelScore5", highScores[4]);
    }
}
