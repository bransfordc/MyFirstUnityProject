﻿#pragma strict

var growRate = 0.005;

function Start () {
    growRate = 0.005;
}

function Update () {
    transform.localScale.x += growRate;
    transform.localScale.y += growRate;

    transform.localPosition.x = Player.hsPosition;

    if( transform.localScale.x > 0.6 ){
        growRate = -0.005;
    }

    if( transform.localScale.x < 0.5 ){
        growRate = 0.005;
    }

}
