﻿#pragma strict


var thisLevel = 0;
var thisLevelHS: UI.Text;



function Start () {
    thisLevelHS = GetComponent.<UI.Text>();
}

function Update () {
    if( Save.highScores[thisLevel - 1] > 0 ){
        thisLevelHS.text = "High Score:  " + Save.highScores[thisLevel - 1];
    } else{
        thisLevelHS.text = "";
    }
}
